using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using Microsoft.SCP;
using Microsoft.SCP.Rpc.Generated;

namespace CoxAutoStorm
{
    public class Spout : ISCPSpout
    {
        private Context ctx;
        private Random rand = new Random();
        string[] sentences = new string[] {
                                    "the cow jumped over the moon",
                                    "an apple a day keeps the doctor away",
                                    "four score and seven years ago",
                                    "snow white and the seven dwarfs",
                                    "i am at two with nature"};

        public Spout(Context ctx, Dictionary<string, Object> parms = null)
        {
            Context.Logger.Info("Spout constructor called");
            this.ctx = ctx;

            Dictionary<string, List<Type>> outputSchema = new Dictionary<string, List<Type>>();
            outputSchema.Add("default", new List<Type>() { typeof(string) });
            this.ctx.DeclareComponentSchema(new ComponentStreamSchema(null, outputSchema));
        }

        public static Spout Get(Context ctx, Dictionary<string, Object> parms)
        {
            return new Spout(ctx);
        }

        public void NextTuple(Dictionary<string, Object> parms)
        {
            string sentence = sentences[rand.Next(0, sentences.Length - 1)];
            Context.Logger.Info("Emit: {0}", sentence);
            this.ctx.Emit(Constants.DEFAULT_STREAM_ID, new Values(sentence));
        }

        public void Ack(long seqId, Dictionary<string, Object> parms)
        {
            // only used in transactional topos
        }

        public void Fail(long seqId, Dictionary<string, Object> parms)
        {
            // only used in transactional topos
        }

    }
}